// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "blanquette" : MessageLookupByLibrary.simpleMessage("Veal stew"),
    "blanquette_i1" : MessageLookupByLibrary.simpleMessage("1.2 kg of veal in pieces"),
    "blanquette_i10" : MessageLookupByLibrary.simpleMessage("1 tablespoon of oil"),
    "blanquette_i11" : MessageLookupByLibrary.simpleMessage("Pepper"),
    "blanquette_i12" : MessageLookupByLibrary.simpleMessage("Salt"),
    "blanquette_i2" : MessageLookupByLibrary.simpleMessage("2 carotts"),
    "blanquette_i3" : MessageLookupByLibrary.simpleMessage("1 onion studded with 1 clove"),
    "blanquette_i4" : MessageLookupByLibrary.simpleMessage("1 leek"),
    "blanquette_i5" : MessageLookupByLibrary.simpleMessage("2 egg yolks"),
    "blanquette_i6" : MessageLookupByLibrary.simpleMessage("100 g of fresh cream"),
    "blanquette_i7" : MessageLookupByLibrary.simpleMessage("1 tablespoon of flour"),
    "blanquette_i8" : MessageLookupByLibrary.simpleMessage("1/2 lemon"),
    "blanquette_i9" : MessageLookupByLibrary.simpleMessage("30 g of butter"),
    "cm" : MessageLookupByLibrary.simpleMessage("Croque Monsieur"),
    "cm_1" : MessageLookupByLibrary.simpleMessage("Sourdough Toast - 2 Slices"),
    "cm_2" : MessageLookupByLibrary.simpleMessage("Gruyere Cheese - 2oz"),
    "cm_3" : MessageLookupByLibrary.simpleMessage("Black Forest Ham - 2 Slices"),
    "cm_4" : MessageLookupByLibrary.simpleMessage("Whole Milk - 1/4 Cup"),
    "cm_5" : MessageLookupByLibrary.simpleMessage("Unsalted Butter - 1tbsp"),
    "cm_6" : MessageLookupByLibrary.simpleMessage("Flour - 1&1/2tsp"),
    "cm_7" : MessageLookupByLibrary.simpleMessage("Salt"),
    "cm_8" : MessageLookupByLibrary.simpleMessage("Pepper"),
    "crepes" : MessageLookupByLibrary.simpleMessage("Crêpes"),
    "crepes_1" : MessageLookupByLibrary.simpleMessage("500 g of flour"),
    "crepes_2" : MessageLookupByLibrary.simpleMessage("1 cup of milk"),
    "crepes_3" : MessageLookupByLibrary.simpleMessage("6 large eggs"),
    "crepes_4" : MessageLookupByLibrary.simpleMessage("1 tablespoon of oil"),
    "frogs" : MessageLookupByLibrary.simpleMessage("Frog\'s legs"),
    "frogs_i1" : MessageLookupByLibrary.simpleMessage("48 frog legs"),
    "frogs_i2" : MessageLookupByLibrary.simpleMessage("50 g of flour"),
    "frogs_i3" : MessageLookupByLibrary.simpleMessage("1/2 bunch of parsley"),
    "frogs_i4" : MessageLookupByLibrary.simpleMessage("3 cloves of garlic"),
    "frogs_i5" : MessageLookupByLibrary.simpleMessage("3 tablespoons of oil"),
    "frogs_i6" : MessageLookupByLibrary.simpleMessage("20 g of butter"),
    "frogs_i7" : MessageLookupByLibrary.simpleMessage("Pepper"),
    "frogs_i8" : MessageLookupByLibrary.simpleMessage("Salt")
  };
}
