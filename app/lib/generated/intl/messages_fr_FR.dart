// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr_FR locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr_FR';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "blanquette" : MessageLookupByLibrary.simpleMessage("Blanquette de veau"),
    "blanquette_i1" : MessageLookupByLibrary.simpleMessage("1.2 kg de veau en morceaux"),
    "blanquette_i10" : MessageLookupByLibrary.simpleMessage("1 cuillère à soupe d\'huile"),
    "blanquette_i11" : MessageLookupByLibrary.simpleMessage("Poivre"),
    "blanquette_i12" : MessageLookupByLibrary.simpleMessage("Sel"),
    "blanquette_i2" : MessageLookupByLibrary.simpleMessage("2 carottes"),
    "blanquette_i3" : MessageLookupByLibrary.simpleMessage("1 oignon piqué d\'1 clou de girofle"),
    "blanquette_i4" : MessageLookupByLibrary.simpleMessage("1 poireau"),
    "blanquette_i5" : MessageLookupByLibrary.simpleMessage("2 jaunes d\'oeuf"),
    "blanquette_i6" : MessageLookupByLibrary.simpleMessage("100 g de crème fraîche"),
    "blanquette_i7" : MessageLookupByLibrary.simpleMessage("1 cuillère à soupe de farine"),
    "blanquette_i8" : MessageLookupByLibrary.simpleMessage("1/2 citron"),
    "blanquette_i9" : MessageLookupByLibrary.simpleMessage("30 g de beurre"),
    "cm" : MessageLookupByLibrary.simpleMessage("Croque Monsieur"),
    "cm_1" : MessageLookupByLibrary.simpleMessage("Toast au levain - 2 Slices"),
    "cm_2" : MessageLookupByLibrary.simpleMessage("Gruyère - 2oz"),
    "cm_3" : MessageLookupByLibrary.simpleMessage("Jambon Forêt Noire - 2 Slices"),
    "cm_4" : MessageLookupByLibrary.simpleMessage("Lait entier - 1/4 Cup"),
    "cm_5" : MessageLookupByLibrary.simpleMessage("Beurre sans sel - 1tbsp"),
    "cm_6" : MessageLookupByLibrary.simpleMessage("Farine - 1&1/2tsp"),
    "cm_7" : MessageLookupByLibrary.simpleMessage("Sel"),
    "cm_8" : MessageLookupByLibrary.simpleMessage("Poivre"),
    "crepes" : MessageLookupByLibrary.simpleMessage("Crêpes"),
    "crepes_1" : MessageLookupByLibrary.simpleMessage("500 g de farine"),
    "crepes_2" : MessageLookupByLibrary.simpleMessage("1 L de lait"),
    "crepes_3" : MessageLookupByLibrary.simpleMessage("6 oeufs"),
    "crepes_4" : MessageLookupByLibrary.simpleMessage("1 cuillère d\'huile"),
    "frogs" : MessageLookupByLibrary.simpleMessage("Cuisses de grenouille"),
    "frogs_i1" : MessageLookupByLibrary.simpleMessage("48 cuisses de grenouille"),
    "frogs_i2" : MessageLookupByLibrary.simpleMessage("50 g de farine"),
    "frogs_i3" : MessageLookupByLibrary.simpleMessage("1/2 bouquet de persil"),
    "frogs_i4" : MessageLookupByLibrary.simpleMessage("3 gousses d\'ail"),
    "frogs_i5" : MessageLookupByLibrary.simpleMessage("3 cuillères à soupe d\'huile"),
    "frogs_i6" : MessageLookupByLibrary.simpleMessage("20 g de beurre"),
    "frogs_i7" : MessageLookupByLibrary.simpleMessage("Poivre"),
    "frogs_i8" : MessageLookupByLibrary.simpleMessage("Sel")
  };
}
