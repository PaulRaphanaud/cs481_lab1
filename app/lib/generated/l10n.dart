// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Veal stew`
  String get blanquette {
    return Intl.message(
      'Veal stew',
      name: 'blanquette',
      desc: '',
      args: [],
    );
  }

  /// `1.2 kg of veal in pieces`
  String get blanquette_i1 {
    return Intl.message(
      '1.2 kg of veal in pieces',
      name: 'blanquette_i1',
      desc: '',
      args: [],
    );
  }

  /// `2 carotts`
  String get blanquette_i2 {
    return Intl.message(
      '2 carotts',
      name: 'blanquette_i2',
      desc: '',
      args: [],
    );
  }

  /// `1 onion studded with 1 clove`
  String get blanquette_i3 {
    return Intl.message(
      '1 onion studded with 1 clove',
      name: 'blanquette_i3',
      desc: '',
      args: [],
    );
  }

  /// `1 leek`
  String get blanquette_i4 {
    return Intl.message(
      '1 leek',
      name: 'blanquette_i4',
      desc: '',
      args: [],
    );
  }

  /// `2 egg yolks`
  String get blanquette_i5 {
    return Intl.message(
      '2 egg yolks',
      name: 'blanquette_i5',
      desc: '',
      args: [],
    );
  }

  /// `100 g of fresh cream`
  String get blanquette_i6 {
    return Intl.message(
      '100 g of fresh cream',
      name: 'blanquette_i6',
      desc: '',
      args: [],
    );
  }

  /// `1 tablespoon of flour`
  String get blanquette_i7 {
    return Intl.message(
      '1 tablespoon of flour',
      name: 'blanquette_i7',
      desc: '',
      args: [],
    );
  }

  /// `1/2 lemon`
  String get blanquette_i8 {
    return Intl.message(
      '1/2 lemon',
      name: 'blanquette_i8',
      desc: '',
      args: [],
    );
  }

  /// `30 g of butter`
  String get blanquette_i9 {
    return Intl.message(
      '30 g of butter',
      name: 'blanquette_i9',
      desc: '',
      args: [],
    );
  }

  /// `1 tablespoon of oil`
  String get blanquette_i10 {
    return Intl.message(
      '1 tablespoon of oil',
      name: 'blanquette_i10',
      desc: '',
      args: [],
    );
  }

  /// `Pepper`
  String get blanquette_i11 {
    return Intl.message(
      'Pepper',
      name: 'blanquette_i11',
      desc: '',
      args: [],
    );
  }

  /// `Salt`
  String get blanquette_i12 {
    return Intl.message(
      'Salt',
      name: 'blanquette_i12',
      desc: '',
      args: [],
    );
  }

  /// `Frog's legs`
  String get frogs {
    return Intl.message(
      'Frog\'s legs',
      name: 'frogs',
      desc: '',
      args: [],
    );
  }

  /// `48 frog legs`
  String get frogs_i1 {
    return Intl.message(
      '48 frog legs',
      name: 'frogs_i1',
      desc: '',
      args: [],
    );
  }

  /// `50 g of flour`
  String get frogs_i2 {
    return Intl.message(
      '50 g of flour',
      name: 'frogs_i2',
      desc: '',
      args: [],
    );
  }

  /// `1/2 bunch of parsley`
  String get frogs_i3 {
    return Intl.message(
      '1/2 bunch of parsley',
      name: 'frogs_i3',
      desc: '',
      args: [],
    );
  }

  /// `3 cloves of garlic`
  String get frogs_i4 {
    return Intl.message(
      '3 cloves of garlic',
      name: 'frogs_i4',
      desc: '',
      args: [],
    );
  }

  /// `3 tablespoons of oil`
  String get frogs_i5 {
    return Intl.message(
      '3 tablespoons of oil',
      name: 'frogs_i5',
      desc: '',
      args: [],
    );
  }

  /// `20 g of butter`
  String get frogs_i6 {
    return Intl.message(
      '20 g of butter',
      name: 'frogs_i6',
      desc: '',
      args: [],
    );
  }

  /// `Pepper`
  String get frogs_i7 {
    return Intl.message(
      'Pepper',
      name: 'frogs_i7',
      desc: '',
      args: [],
    );
  }

  /// `Salt`
  String get frogs_i8 {
    return Intl.message(
      'Salt',
      name: 'frogs_i8',
      desc: '',
      args: [],
    );
  }

  /// `Croque Monsieur`
  String get cm {
    return Intl.message(
      'Croque Monsieur',
      name: 'cm',
      desc: '',
      args: [],
    );
  }

  /// `Sourdough Toast - 2 Slices`
  String get cm_1 {
    return Intl.message(
      'Sourdough Toast - 2 Slices',
      name: 'cm_1',
      desc: '',
      args: [],
    );
  }

  /// `Gruyere Cheese - 2oz`
  String get cm_2 {
    return Intl.message(
      'Gruyere Cheese - 2oz',
      name: 'cm_2',
      desc: '',
      args: [],
    );
  }

  /// `Black Forest Ham - 2 Slices`
  String get cm_3 {
    return Intl.message(
      'Black Forest Ham - 2 Slices',
      name: 'cm_3',
      desc: '',
      args: [],
    );
  }

  /// `Whole Milk - 1/4 Cup`
  String get cm_4 {
    return Intl.message(
      'Whole Milk - 1/4 Cup',
      name: 'cm_4',
      desc: '',
      args: [],
    );
  }

  /// `Unsalted Butter - 1tbsp`
  String get cm_5 {
    return Intl.message(
      'Unsalted Butter - 1tbsp',
      name: 'cm_5',
      desc: '',
      args: [],
    );
  }

  /// `Flour - 1&1/2tsp`
  String get cm_6 {
    return Intl.message(
      'Flour - 1&1/2tsp',
      name: 'cm_6',
      desc: '',
      args: [],
    );
  }

  /// `Salt`
  String get cm_7 {
    return Intl.message(
      'Salt',
      name: 'cm_7',
      desc: '',
      args: [],
    );
  }

  /// `Pepper`
  String get cm_8 {
    return Intl.message(
      'Pepper',
      name: 'cm_8',
      desc: '',
      args: [],
    );
  }

  /// `Crêpes`
  String get crepes {
    return Intl.message(
      'Crêpes',
      name: 'crepes',
      desc: '',
      args: [],
    );
  }

  /// `500 g of flour`
  String get crepes_1 {
    return Intl.message(
      '500 g of flour',
      name: 'crepes_1',
      desc: '',
      args: [],
    );
  }

  /// `1 cup of milk`
  String get crepes_2 {
    return Intl.message(
      '1 cup of milk',
      name: 'crepes_2',
      desc: '',
      args: [],
    );
  }

  /// `6 large eggs`
  String get crepes_3 {
    return Intl.message(
      '6 large eggs',
      name: 'crepes_3',
      desc: '',
      args: [],
    );
  }

  /// `1 tablespoon of oil`
  String get crepes_4 {
    return Intl.message(
      '1 tablespoon of oil',
      name: 'crepes_4',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'fr', countryCode: 'FR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}