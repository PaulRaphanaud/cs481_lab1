import 'package:app/generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:app/Widgets/Receipe.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onRecipe1Pressed() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).frogs),
            scrollable: true,
            content: Container(
              height: MediaQuery.of(context).size.width / 1.5,
              width: MediaQuery.of(context).size.width / 1.5,
              child: ListView(
                children: <Widget>[
                  Text(S.of(context).frogs_i1),
                  Text(S.of(context).frogs_i2),
                  Text(S.of(context).frogs_i3),
                  Text(S.of(context).frogs_i4),
                  Text(S.of(context).frogs_i5),
                  Text(S.of(context).frogs_i6),
                  Text(S.of(context).frogs_i7),
                  Text(S.of(context).frogs_i8),
                ],
                shrinkWrap:
                    true, // les ingredients tu met photo + nom ? ou juste nom
              ),
            ),
          );
        });
  }

  void information() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text("App info"),
            content: Container(
              child: Text("This application contains several receipe"),
            ),
          );
        });
  }

  void onRecipe2Pressed() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).blanquette),
            scrollable: true,
            content: Container(
              height: MediaQuery.of(context).size.width / 1.5,
              width: MediaQuery.of(context).size.width / 1.5,
              child: ListView(
                children: <Widget>[
                  Text(S.of(context).blanquette_i1),
                  Text(S.of(context).blanquette_i2),
                  Text(S.of(context).blanquette_i3),
                  Text(S.of(context).blanquette_i4),
                  Text(S.of(context).blanquette_i5),
                  Text(S.of(context).blanquette_i6),
                  Text(S.of(context).blanquette_i7),
                  Text(S.of(context).blanquette_i8),
                  Text(S.of(context).blanquette_i9),
                  Text(S.of(context).blanquette_i10),
                  Text(S.of(context).blanquette_i11),
                  Text(S.of(context).blanquette_i12),
                ],
                shrinkWrap: true,
              ),
            ),
          );
        });
  }

  void onRecipe3Pressed() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).crepes),
            scrollable: true,
            content: Container(
              height: MediaQuery.of(context).size.width / 1.5,
              width: MediaQuery.of(context).size.width / 1.5,
              child: ListView(
                children: <Widget>[
                  Text(S.of(context).crepes_1),
                  Text(S.of(context).crepes_2),
                  Text(S.of(context).crepes_3),
                  Text(S.of(context).crepes_4),
                ],
                shrinkWrap: true,
              ),
            ),
          );
        });
  }

  void onRecipe4Pressed() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(S.of(context).cm),
            scrollable: true,
            content: Container(
              height: MediaQuery.of(context).size.width / 1.5,
              width: MediaQuery.of(context).size.width / 1.5,
              child: ListView(
                children: <Widget>[
                  Text(S.of(context).cm_1),
                  Text(S.of(context).cm_2),
                  Text(S.of(context).cm_3),
                  Text(S.of(context).cm_4),
                  Text(S.of(context).cm_5),
                  Text(S.of(context).cm_6),
                  Text(S.of(context).cm_7),
                  Text(S.of(context).cm_8),
                ],
                shrinkWrap: true,
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.info_outline), onPressed: information),
      body: Container(
        alignment: Alignment.center,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                  onTap: onRecipe1Pressed,
                  child: Receipe(
                      path_image: "./assets/images/frogs.jpg",
                      title: S.of(context).frogs)
              ),
              SizedBox(height: MediaQuery.of(context).size.height/25),
              GestureDetector(
                onTap: onRecipe2Pressed,
                child: Receipe(
                    path_image: "./assets/images/blanquette.jpg",
                    title: S.of(context).blanquette),
              ),
              SizedBox(height: MediaQuery.of(context).size.height/25),
              GestureDetector(
                  onTap: onRecipe3Pressed,
                  child: Receipe(
                      path_image: "./assets/images/crepes.jpg",
                      title: S.of(context).crepes),
              ),
              SizedBox(height: MediaQuery.of(context).size.height/25),
              GestureDetector(
                onTap: onRecipe4Pressed,
                child: Receipe(
                    path_image: "./assets/images/croque-monsieur.jpg",
                    title: S.of(context).cm),
              ),
            ]),
      ),
    );
  }
}
