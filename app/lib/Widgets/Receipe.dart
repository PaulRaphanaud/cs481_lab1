import 'package:flutter/material.dart';

class Receipe extends StatelessWidget {
  Receipe({Key key, @required this.path_image, @required this.title})
      : super(key: key);
  final String path_image;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.25,
      height: 75,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5), bottomLeft: Radius.circular(5)),
            child: Image.asset(path_image, fit: BoxFit.cover, width: 100, height: 75),
          ),
          Expanded(child: Center(child: Text(title))),
        ],
      ),
    );
  }
}
